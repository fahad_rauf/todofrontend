<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE HTML>
<html lang="en">
	<head>
		<title>todo</title>
	</head>
	<body>
		<div id="todos">
			<h1>todos</h1>
			<div class="content">
				<input type="text" class="create" placeholder="What needs to be done?">
			</div>
			<ul id="list"></ul>
			<div id="todo-stats"></div>
		</div>
		<ul id="instructions">
		  <li>Double-click to edit a todo.</li>
		</ul>
		<script type='text/javascript' src='${pageContext.request.contextPath}/resources/javascriptmvc/steal/steal.production.js?todo'></script>
		<a href="<c:url value="/j_spring_security_logout" />" > Logout</a>
	</body>
</html>

